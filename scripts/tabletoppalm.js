var jqueryNoConflict = jQuery;

// begin main function
jqueryNoConflict(document).ready(function(){

    initializeTabletopObject('0Aux9LJOg77smdGZURHgtVU85YmQ0UHJteFNzbXVib3c');

});

// pull data from google spreadsheet
function initializeTabletopObject(dataSpreadsheet){
    Tabletop.init({
        key: dataSpreadsheet,
        callback: writeTableWith,
        simpleSheet: true,
        debug: false
    });
}

// create table headers
function createTableColumns(){

    /* swap out the properties of mDataProp & sTitle to reflect
    the names of columns or keys you want to display.
    Remember, tabletop.js strips out spaces from column titles, which
    is what happens with the More Info column header */

    var tableColumns =   [
		{'mDataProp': 'palm', 'sTitle': 'Rank', 'sClass': 'center'},
		{'mDataProp': 'player', 'sTitle': 'Athlete', 'sClass': 'center'},
		{'mDataProp': 'pos', 'sTitle': 'Position', 'sClass': 'center'},
		{'mDataProp': 'hs', 'sTitle': 'High School', 'sClass': 'center'},
		{'mDataProp': 'college', 'sTitle': 'College', 'sClass': 'center'},
		{'mDataProp': 'comment', 'sTitle': 'Comment', 'sClass': 'center'}
	];
    return tableColumns;
}

// create the table container and object
function writeTableWith(dataSource){

    jqueryNoConflict('#palm').html('<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered table-striped" id="data-table-containera"></table>');
posi
    var oTable = jqueryNoConflict('#data-table-containera').dataTable({
		'sPaginationType': 'bootstrap',
		'iDisplayLength': 25,
        'aaData': dataSource,
        'aoColumns': createTableColumns(),
        'oLanguage': {
            'sLengthMenu': '_MENU_ records per page'
        }
    });
};

//define two custom functions (asc and desc) for string sorting
jQuery.fn.dataTableExt.oSort['string-case-asc']  = function(x,y) {
	return ((x < y) ? -1 : ((x > y) ?  0 : 0));
};

jQuery.fn.dataTableExt.oSort['string-case-desc'] = function(x,y) {
	return ((x < y) ?  1 : ((x > y) ? -1 : 0));
};




